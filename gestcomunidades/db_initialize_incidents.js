var async = require('async')
var Incident = require('./models/Incident')

// Mongoose connection
var mongoose = require('mongoose');
var mongoDB = 'mongodb://admin:gestcomunidades_admin_pass@ds161148.mlab.com:61148/db_gestcomunidades';
mongoose.connect(mongoDB);
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error'))


var incidents = []


function incidentCreate(community, user, title,
                         comment, date, cb){
    var incident = new Incident({community: community,
    user:user, title:title, date:date, comment:comment});
    incident.save(function (err){
        if (err){
            cb(err,null)
            return
        }
        console.log('New incident: ' + incident);
        incidents.push(incident);
        cb(null,incident);
    });
}


function createIncident(cb){
    async.parallel([
        function(callback) {
          incidentCreate('5aa12e0c6188ba0b385ad2e6', '5aa12e0d6188ba0b385ad2ea', 'Suelo roto', 
            'Hay un roto en la segunda planta y puede ser peligroso  para los vecinos, arreglarlo lo antes posible', new Date(),callback);
        },
        function(callback) {
          incidentCreate('5aa12e0c6188ba0b385ad2e6', '5aa12e0d6188ba0b385ad2ea', 'Puerta bloqueada', 
            'La puerta del portal está bloqueada y no se abre, por favor avisar a mantenimiento..',new Date(), callback);
        }
        ],
        // optional callback
        cb);
}


Incident.collection.drop();

async.series([
    createIncident
],
// Optional callback
function(err, results) {
    if (err) {
        console.log('FINAL ERR: '+err);
    }
    else {
        console.log('Incidents: '+ incidents);
        
    }
    // All done, disconnect from database
    mongoose.connection.close();
});