var async = require('async');
var Incident = require('../models/incident');

// list_services in community
exports.incidents_in_community = function(req, res){	
	Incident.find({community:req.user_data.communityId})
		.sort([['date','ascending']])
		.exec(function(err, list_incidents){
			if(err) { return next(err); }
			res.render('incidents_in_community', { title: 'Incidents List', incidents_list : list_incidents});
		});
}
