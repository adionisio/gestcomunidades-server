var async = require('async');
var Notice = require('../models/notice');

// list_services in community
exports.notices_in_community = function(req, res){	
	Notice.find({community:req.user_data.communityId})
		.sort([['date','ascending']])
		.exec(function(err, list_notices){
			if(err) { return next(err); }
			res.render('notices_in_community', { title: 'Notice List', notice_list: list_notices});
		});
}
