var async = require('async');
var User = require('../models/user');
var jwt = require('jsonwebtoken');
var config = require('../config');
var fs = require('fs');


//GET login
exports.login_form = function(req, res, next) {
  res.render('login', {});
}

// POST login
exports.login =  function(req, res, next) {
	User.findOne({email:req.body.username})
		.exec(function(err, user){
			if(err) { return next(err); }
	        user.comparePassword(req.body.pass, function(err, isMatch) {
	            if (err) throw err;
	            console.log(req.query.pass, isMatch);
	            if (isMatch){
					const payload = {
					      userId: user._id,
					      communityId: user.community
					    };	           
			        var token = jwt.sign(payload, config.secret, {
			          expiresIn: 60 * 15 
			        });				
			        res.cookie('auth',token);
	            	res.redirect('/user');            	
	            }else
	            	res.render('login', {error: "Usuario o password incorrecto."});
	        });				
		});  
}

exports.check_token = function(req, res, next) {

  var token = req.cookies.auth || req.get("auth");
  if (token) {

    jwt.verify(token, config.secret, function(err, token_data) {
      if (err) {
         res.clearCookie("auth");
         return res.render('login', {error: "Su sesión ha expirado, entre de nuevo."});
      } else {
        req.user_data = token_data;
        next();
      }
    });

  } else {
    return res.render('login', {error: "Acceso prohibido. Inserte usuario"});
  }
}



// Users list website render frontend
exports.user_list = function(req, res){
	User.find()
		.sort([['floor','ascending']])
		.select('-password')
		.exec(function(err, list_users){
			if(err) { return next(err); }
			res.render('user_list', { title: 'User List', user_list: list_users});
		});
}


// User detail
exports.user_detail = function(req, res){	
	console.log("Del token pillamos " + req.user_data);
	User.findById(req.user_data.userId)
		.select('-password')
		.exec(function(err, user){
			if(err) { return next(err); }
			res.render('user_detail', { title: 'User detail', user: user});
		});
}

// Users in community
exports.users_in_community = function(req, res){	
	User.find({community:req.user_data.communityId, role: "5aa12e0d6188ba0b385ad2e8"})
		.select('-password')
		.sort([['floor','ascending']])
		.exec(function(err, list_users){
			if(err) { return next(err); }
			res.render('users_in_community', { title: 'User List', user_list: list_users});
		});
}

exports.upload_user_image = function(req, res, next) {
  res.render('upload', { title: 'Express' });
}

exports.upload_user_image_post = function(req, res, next) {
	console.log(req.body);
	console.log(req.file); //form files	
	var image_origin = req.file.originalname.split(".");
	var extension = image_origin[image_origin.length-1];
	console.log(req.file.destination);
	fs.rename(req.file.destination + req.file.filename,
	req.file.destination + req.body.userid + "." + extension , function(err) {
    	if ( err ) console.log('ERROR: ' + err);
	});	
	res.json(req.file);
}

exports.logout = function(req,res){
	res.clearCookie("auth");
	res.redirect('/');
}
