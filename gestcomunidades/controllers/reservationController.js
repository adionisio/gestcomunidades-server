var async = require('async');
var Reservation = require('../models/reservation');
var Service = require('../models/service');

exports.reservations = function (req, res) {

    async.parallel({
        list_reservations: function(callback) {
            Reservation.find({user:req.user_data.userId})
				.sort([['date_in','descending']])
				.populate('service')
				.exec(callback);
        },
        list_services: function(callback) {
          Service.find({ 'community': req.user_data.communityId, 'bookable': true })
          .exec(callback);
        },
    }, function(err, results) {
        if (err) { return next(err); }
        res.render('reservations', 
        	{ 
        		reservation_list:results.list_reservations, 
        	  	service_list:results.list_services
        	});
    });
}

exports.reservations_post = function (req, res) {

    // Create an reservation object
    console.log(req.body);
    var reservation = new Reservation(
        {
            community: req.user_data.communityId,
            user: req.user_data.userId,
            service: req.body.service,
            date_in: req.body.date_in,
            date_out: req.body.date_out
        });
    reservation.save(function (err) {
        if (err) { res.json({error: "Error creating reservation.", err: err}); }
	    async.parallel({
	        list_reservations: function(callback) {
	            Reservation.find({user:req.user_data.userId})
					.sort([['date_in','ascending']])
					.populate('service')
					.exec(callback);
	        },
	        list_services: function(callback) {
	          Service.find({ 'community': req.user_data.communityId, 'bookable': true })
	          .exec(callback);
	        },
	    }, function(err, results) {
	        if (err) { return next(err); }
	        res.render('reservations', 
	        	{ 
	        		reservation_list:results.list_reservations, 
	        	  	service_list:results.list_services
	        	});
	   		 });
    });		    
}
