var async = require('async');

var Community = require('../../../models/community');


// Communities list json API
exports.community_list = function(req, res){
	Community.find()
		.exec(function(err, list_communitys){
			if(err) { return next(err); }
			res.json(list_communitys);
		});		
}


// community detail
exports.community_detail = function(req, res){	
	Community.findById(req.params.id)
		.exec(function(err, community){
			if(err) { return next(err); }
			res.json(community);
		});
}

// DELETE

// Delete community 
exports.community_delete = function(req, res){	
	Community.findByIdAndRemove(req.params.id)
		.exec(function(err){
			if(err) { return next(err); }
			res.json({delete:true});
		});
}

// POST

// Create community

exports.community_create = function(req,res){
    // Create an Community object
    var community = new Community(
        {
            name: req.body.name,
            address: req.body.address
        });
    community.save(function (err) {
        if (err) { res.json({error: "Error creating community.", err: err}); }
        res.json(community.url);
    });	
}