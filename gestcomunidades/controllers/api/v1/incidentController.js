var async = require('async');
var Incident = require('../../../models/incident');
var fs = require('fs');
var multer  = require('multer');
var imgIncidentsPath = '../../../public/images/incidents/';
var uploadIncidents = multer({ dest: imgIncidentsPath });




// Incidents list json API
exports.incident_list = function(req, res){
	Incident.find()
		.exec(function(err, list_incidents){
			if(err) { return next(err); }
			res.json(list_incidents);
		});		
}


// incident detail
exports.incident_detail = function(req, res){	
	Incident.findById(req.params.id)
		.exec(function(err, incident){
			if(err) { return next(err); }
			res.json(incident);
		});
}

// Incidents list from community json API
exports.incident_by_community = function(req, res){
	Incident.find({community:req.params.id})
		.sort([['date','descending']])
		.exec(function(err, list_incidents){
			if(err) { return next(err); }
			res.json(list_incidents);
		});		
}

// Incidents list from community and user json API
exports.incident_by_community_user = function(req, res){
	Incident.find({community:req.params.id, user:req.params.iduser})
		.sort([['date','descending']])
		.exec(function(err, list_incidents){
			if(err) { return next(err); }
			res.json(list_incidents);
		});		
}


// DELETE

// Delete incident 
exports.incident_delete = function(req, res){	
	Incident.findByIdAndRemove(req.params.id)
		.exec(function(err){
			if(err) { return next(err); }
			res.json({delete:true});
		});
}

// Create incident

exports.incident_create = function(req,res){
    // Create an incident object
    console.log(req.body);
    var incident = new Incident(
        {
            community: req.body.community,
            user: req.body.user,
            title: req.body.title,
            comment: req.body.comment,
            date: new Date()
        });
    incident.save(function (err) {
        if (err) { res.json({error: "Error creating incident.", err: err}); }
        res.json(incident.url);
    });	
}

// Save incident with photo

exports.incident_create_photo = function(req,res,next){
    // Create an incident object
    var incident = new Incident(
        {
            community: req.body.idCommunity,
            user: req.body.idUser,
            title: req.body.title,
            comment: req.body.comment,
            date: new Date()
        });
    incident.save(function (err) {
        if (err) { res.json({error: "Error creating incident.", err: err}); }
		var image_origin = req.file.originalname.split(".");
		var extension = image_origin[image_origin.length-1];
		fs.rename(req.file.destination + req.file.filename,
		req.file.destination + incident._id + "." + extension , function(err) {
	    	if ( err ) console.log('ERROR: ' + err);
		});	
		res.json(incident.url);
    });	
}

