var async = require('async');
var Service = require('../../../models/service');


// Services list json API
exports.service_list = function(req, res){
	Service.find()
		.exec(function(err, list_services){
			if(err) { return next(err); }
			res.json(list_services);
		});		
}


// Services per community
exports.services_by_community = function(req, res){
	Service.find({community:req.params.id})
		.exec(function(err, list_services){
		if(err) { return next(err); }
		res.json(list_services);
	});		
}

// Services bookables per community
exports.services_bookable_by_community = function(req, res){
	Service.find({community:req.params.id,bookable:true})
		.exec(function(err, list_services){
		if(err) { return next(err); }
		res.json(list_services);
	});		
}


// service detail
exports.service_detail = function(req, res){	
	Service.findById(req.params.id)
		.exec(function(err, service){
			if(err) { return next(err); }
			res.json(service);
		});
}

// DELETE

// Delete service 
exports.service_delete = function(req, res){	
	Service.findByIdAndRemove(req.params.id)
		.exec(function(err){
			if(err) { return next(err); }
			res.json({delete:true});
		});
}

// POST

// Create service

exports.service_create = function(req,res){
    // Create an Service object
	var timetable_body = {
		open_hour: req.body.open_hour,
		open_minute: req.body.open_minute,
		close_hour: req.body.close_hour,
		close_minute: req.body.close_minute
	}

    var service = new Service(
        {
            community: req.body.community,
            name: req.body.name,
            timetable: timetable_body,
            bookable: req.body.bookable
        });
    service.save(function (err) {
        if (err) { res.json({error: "Error creating service.", err: err}); }
        res.json(service.url);
    });	
}

