var async = require('async');
var Role = require('../../../models/role');


// Roles list json API
exports.role_list = function(req, res){
	Role.find()
		.exec(function(err, list_roles){
			if(err) { return next(err); }
			res.json(list_roles);
		});		
}


// Role detail
exports.role_detail = function(req, res){	
	Role.findById(req.params.id)
		.exec(function(err, role){
			if(err) { return next(err); }
			res.json(role);
		});
}

// DELETE

// Delete role 
exports.role_delete = function(req, res){	
	Role.findByIdAndRemove(req.params.id)
		.exec(function(err){
			if(err) { return next(err); }
			res.json({delete:true});
		});
}



// Create role

exports.role_create = function(req,res){
    // Create an role object
    console.log(req.body);
    var role = new Role(
        {
            category: req.body.category,
        });
    role.save(function (err) {
        if (err) { res.json({error: "Error creating role.", err: err}); }
        res.json(role.url);
    });	
}
