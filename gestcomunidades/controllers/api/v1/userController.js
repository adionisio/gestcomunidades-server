var async = require('async');
var bcrypt = require('bcrypt');
var User = require('../../../models/user');
var jwt = require('jsonwebtoken');
var config = require('../../../config');



// GET

// Users list json API
exports.user_list = function(req, res){
	User.find()
		.sort([['floor','ascending']])
		.select('-password')
		.exec(function(err, list_users){
			if(err) { return next(err); }
			res.json(list_users);
		});		
}


// User detail
exports.user_detail = function(req, res){	
	User.findById(req.params.id)
		.select('-password')
		.exec(function(err, user){
			if(err) { return next(err); }
			res.json(user);
		});
}

// User detail administrator
exports.user_detail_administrator = function(req, res){	
	User.findOne({role:"5aa12e0d6188ba0b385ad2e7"})
		.select('-password')
		.exec(function(err, user){
			if(err) { return next(err); }
			console.log("Headers: ");
			console.log(req.get("auth"));
			res.json(user);
		});
}

// User detail doorman
exports.user_detail_doorman = function(req, res){	
	User.findOne({role:"5aa12e0d6188ba0b385ad2e9"})
		.select('-password')
		.exec(function(err, user){
			if(err) { return next(err); }			
			res.json(user);
		});
}

// User detail pass
exports.user_detail_pass = function(req, res){	
	User.findOne({email:req.params.email})
		.exec(function(err, user){
			if(err) { return next(err); }
			console.log("reqbody: " + req.query.pass);
	        user.comparePassword(req.query.pass, function(err, isMatch) {
	            if (err) throw err;
	            console.log(req.query.pass, isMatch);
	            if (isMatch){
	            	user.password = "";
	            	res.json(user);		
	            }
	            else
	            	res.status(404).send('Not found');
	        });			
			
		});
}

// Users per community
exports.users_by_community = function(req, res){
	User.find({community:req.params.id , role: "5aa12e0d6188ba0b385ad2e8" })
		.sort([['floor','ascending']])
		.select('-password')
		.exec(function(err, list_users){
		if(err) { return next(err); }
		res.json(list_users);
	});		
}

// DELETE

// Delete user 
exports.user_delete = function(req, res){	
	User.findByIdAndRemove(req.params.id)
		.exec(function(err){
			if(err) { return next(err); }
			res.json({delete:true});
		});
}

// Create user

exports.user_create = function(req,res){
    // Create an user object
    var user = new User(
        {
            community: req.body.community,
            name: req.body.name,
            surname: req.body.surname,
            password: req.body.password,
            email: req.body.email,
            address: req.body.address,
            floor: req.body.floor,
            letter: req.body.letter,
            phone: req.body.phone,
            role: req.body.role
        });
    user.save(function (err) {
        if (err) { res.json({error: "Error creating user.", err: err}); }
        res.json(user.url);
    });	
}


// Authenticate with token
exports.user_token_authenticate = function(req,res){
	User.findOne({email:req.body.userEmail})
		.exec(function(err, user){
			if(err) { return next(err); }
			if(user){
				console.log("reqbody: " + req.body.pass);
		        user.comparePassword(req.body.pass, function(err, isMatch) {
		            if (err) throw err;
		            console.log(req.body.pass, isMatch);
		            if (isMatch){
						const payload = {
						      userId: user._id
						    };	           
				        var token = jwt.sign(payload, config.secret, {
				          expiresIn: 60 * 60 * 24 * 365 
				        });				
				        user.password = "";	 
				        res.json({
				          success: true,
				          token: token,
				          user: user
				        });
		            }
		            else
		            	res.status(404).send('Not found');
		        });	
	        }else{
        		res.status(404).send('Not found');
	        }		
			
		});	
}

exports.check_token = function(req, res, next) {

  var token = req.cookies.auth || req.get("auth");
  if (token) {

    jwt.verify(token, config.secret, function(err, token_data) {
      if (err) {
         res.clearCookie("auth");
         return res.json({error: "Su sesión ha expirado, entre de nuevo."});
      } else {
        req.user_data = token_data;
        next();
      }
    });

  } else {
    return res.json({error: "Acceso prohibido. Inserte usuario"});
  }
}
