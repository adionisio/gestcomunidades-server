var async = require('async');
var Notice = require('../../../models/notice');


// Notices list json API
exports.notice_list = function(req, res){
	Notice.find()
		.exec(function(err, list_notices){
			if(err) { return next(err); }
			res.json(list_notices);
		});		
}


// notice detail
exports.notice_detail = function(req, res){	
	Notice.findById(req.params.id)
		.exec(function(err, notice){
			if(err) { return next(err); }
			res.json(notice);
		});
}


// Notices per community
exports.notices_by_community = function(req, res){
	Notice.find({community:req.params.id})
		.sort([['date','descending']])
		.exec(function(err, list_notices){
		if(err) { return next(err); }
		res.json(list_notices);
	});		
}

// DELETE

// Delete notice 
exports.notice_delete = function(req, res){	
	Notice.findByIdAndRemove(req.params.id)
		.exec(function(err){
			if(err) { return next(err); }
			res.json({delete:true});
		});
}


// Create notice

exports.notice_create = function(req,res){
    // Create an notice object
    console.log(req.body);
    var notice = new Notice(
        {
            community: req.body.community,
            title: req.body.title,
            comment: req.body.comment,
            date: Date.now()
        });
    notice.save(function (err) {
        if (err) { res.json({error: "Error creating notice.", err: err}); }
        res.json(notice.url);
    });	
}
