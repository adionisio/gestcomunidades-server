var async = require('async');
var Reservation = require('../../../models/reservation');


// Reservations list json API
exports.reservation_list = function(req, res){
	Reservation.find()
		.sort([['date_in','ascending']])
		.exec(function(err, list_reservations){
			if(err) { return next(err); }
			res.json(list_reservations);
		});		
}


// Reservations per community
exports.reservations_by_community = function(req, res){
	Reservation.find({community:req.params.id})
		.sort([['date_in','descending']])
		.populate('service')
		.exec(function(err, list_reservations){
		if(err) { return next(err); }
		res.json(list_reservations);
	});		
}

// Reservations per community in date
exports.reservations_by_community_service_date = function(req, res){
	date_min = new Date(req.params.date);
	console.log(date_min.toISOString());
	date_max = new Date(req.params.date);
	date_max.setDate(date_max.getDate()+1);
	console.log(date_max.toISOString());
	Reservation.find({community:req.params.id, service:req.params.service, date_in:{$gte:date_min,$lte:date_max}})
		.populate('service')
		.sort([['date_in','descending']])
		.exec(function(err, list_reservations){
		if(err) { return next(err); }
		res.json(list_reservations);
	});		
}

// Reservations per community in date
exports.check_reservations_by_community_service_date = function(req, res){
	console.log("Check dates");
	date_min = new Date(req.query.dateIn);
	date_min.setTime(date_min.getTime()+2*60*60*1000);
	console.log(date_min.toISOString());
	date_max = new Date(req.query.dateOut);
	date_max.setTime(date_max.getTime()+2*60*60*1000);
	console.log(date_max.toISOString());
	Reservation.find({community:req.params.id, service:req.params.service, date_in:{$gte:date_min,$lte:date_max}})
		.populate('service')
		.sort([['date_in','ascending']])
		.exec(function(err, list_reservations){
		if(err) { return next(err); }
		res.json(list_reservations);
	});	
}

// Reservations per user
exports.reservations_by_user = function(req, res){
	Reservation.find({user:req.params.id})
		.populate('service')
		.sort([['date_in','descending']])
		.exec(function(err, list_reservations){
		if(err) { return next(err); }
		res.json(list_reservations);
	});		
}


// reservation detail
exports.reservation_detail = function(req, res){	
	Reservation.findById(req.params.id)
		.exec(function(err, reservation){
			if(err) { return next(err); }
			res.json(reservation);
		});
}

// DELETE

// Delete reservation 
exports.reservation_delete = function(req, res){	
	Reservation.findByIdAndRemove(req.params.id)
		.exec(function(err){
			if(err) { return next(err); }
			res.json({delete:true});
		});
}

// Create reservation

exports.reservation_create = function(req,res){
    // Create an reservation object
    console.log(req.body);
    var reservation = new Reservation(
        {
            community: req.body.community,
            user: req.body.user,
            service: req.body.service,
            date_in: req.body.date_in,
            date_out: req.body.date_out
        });
    reservation.save(function (err) {
        if (err) { res.json({error: "Error creating reservation.", err: err}); }
        res.json(reservation.url);
    });	
}
