var async = require('async');
var Service = require('../models/service');

// list_services in community
exports.services_in_community = function(req, res){	
	Service.find({community:req.user_data.communityId})
		.sort([['name','ascending']])
		.exec(function(err, list_services){
			if(err) { return next(err); }
			res.render('services_in_community', { title: 'Serviec List', service_list: list_services});
		});
}
