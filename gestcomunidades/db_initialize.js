var async = require('async')
var Community = require('./models/community')
var Role = require('./models/role')
var User = require('./models/user')
var Service = require('./models/Service')
var Reservation = require('./models/Reservation')
var Incident = require('./models/Incident')
var Notice = require('./models/Notice')

// Mongoose connection
var mongoose = require('mongoose');
var mongoDB = 'mongodb://admin:gestcomunidades_admin_pass@ds161148.mlab.com:61148/db_gestcomunidades';
mongoose.connect(mongoDB);
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error'))

var communities = []
var roles = []
var users = []
var services = []
var reservations = []
var incidents = []
var notices = []

function communityCreate(name, address, cb){
	var community = new Community({name: name, address: address});
	community.save(function (err){
		if (err){
			cb(err,null)
			return
		}
		console.log('New community: ' + community);
		communities.push(community);
		cb(null,community);
	});
}

function roleCreate(category, cb){
	var role = new Role({category: category});
	role.save(function (err){
		if (err){
			cb(err,null)
			return
		}
		console.log('New role: ' + role);
		roles.push(role);
		cb(null,role);
	});
}

function userCreate(community, name, surname, password, email, address, floor, letter, phone, role, cb){
	var user = new User({community: community,
	name:name, surname:surname, password:password, email:email, address: address,
	floor:floor, letter:letter, phone:phone, role:role});
	user.save(function (err){
		if (err){
			cb(err,null)
			return
		}
		console.log('New user: ' + user);
		users.push(user);
		cb(null,user);
	});
}

function serviceCreate(community,name, open_hour, open_minute,
                         close_hour, close_minute,bookable, cb){
    var service = new Service(
        {
            community: community,
            name: name,
            timetable: {
                open_hour:open_hour,
                open_minute:open_minute,
                close_hour:close_hour,
                close_minute:close_minute 
            },
            bookable: bookable
        }
    );
    service.save(function (err){
        if (err){
            cb(err,null)
            return
        }
        console.log('New service: ' + service);
        services.push(service);
        cb(null,service);
    });
}


function reservationCreate(community, user, service,
                         date_in, date_out, cb){
    var reservation = new Reservation({community: community,
    user:user, service:service, date_in:date_in, date_out:date_out});
    reservation.save(function (err){
        if (err){
            cb(err,null)
            return
        }
        console.log('New reservation: ' + reservation);
        reservations.push(reservation);
        cb(null,reservation);
    });
}

function incidentCreate(community, user, title,
                         comment, cb){
    var incident = new Incident({community: community,
    user:user, title:title, comment:comment});
    incident.save(function (err){
        if (err){
            cb(err,null)
            return
        }
        console.log('New incident: ' + incident);
        incidents.push(incident);
        cb(null,incident);
    });
}


function createCommunity(cb) {
    async.parallel([
        function(callback) {
          communityCreate('Los Alamos', 'Calle Gran via 12 28043 Madrid', callback);
        }
        ],
        // optional callback
        cb);
}

function createRole(cb){
    async.parallel([
        function(callback) {
          roleCreate('Administrator', callback);
        },
        function(callback) {
          roleCreate('Resident', callback);
        },
        function(callback) {
          roleCreate('Doorman', callback);
        }               
        ],
        // optional callback
        cb);
}

function createUser(cb){
    async.parallel([
        function(callback) {
          userCreate(communities[0], 'Alejandro', 'Martinez Trujillo', '123135*45ADesgtE', 
          'amartinez@gmail.com', 'Calle Gran Via 12 28043 Madrid', 4, 'A', 616055225, roles[0], callback);
        },
        function(callback) {
          userCreate(communities[0], 'Irene', 'Garcia Benito', '123135*45ADesgtE', 
          'garciairene@gmail.com', 'Calle Gran Via 12 28043 Madrid', 3, 'B', 619087212, roles[1], callback);
        }
        ],
        // optional callback
        cb);
}

function createService(cb){
    async.parallel([
        function(callback) {
          serviceCreate(communities[0], 'Pool', 10, 0, 20, 30, false, callback);
        },
        function(callback) {
          serviceCreate(communities[0], 'Paddel', 11, 0, 21, 0, true, callback);
        },
        function(callback) {
          serviceCreate(communities[0], 'Local', 11, 0, 21, 0, true, callback);
        }
        ],
        // optional callback
        cb);
}

function createReservation(cb){
    async.parallel([
        function(callback) {
          reservationCreate(communities[0], users[0], services[1], 
            new Date('2018-03-15T16:30:00Z'), new Date('2018-03-15T17:30:00Z'), callback);
        },
        function(callback) {
          reservationCreate(communities[0], users[1], services[1], 
            new Date('2018-03-15T19:00:00Z'), new Date('2018-03-15T20:00:00Z'), callback);
        },
        function(callback) {
          reservationCreate(communities[0], users[0], services[2], 
            new Date('2018-03-15T16:30:00Z'), new Date('2018-03-15T17:30:00Z'), callback);
        }
        ],
        // optional callback
        cb);
}

function createIncident(cb){
    async.parallel([
        function(callback) {
          incidentCreate(communities[0], users[0], 'Second floor is broken', 
            'I found a little hole in second floor, maybe is dangerous for the community, please fix it as soon as possible', callback);
        },
        function(callback) {
          incidentCreate(communities[0], users[1], 'Door locked', 
            'The street door is locked! I can not open it and it is a big problem.', callback);
        }
        ],
        // optional callback
        cb);
}

Community.collection.drop();
Role.collection.drop();
User.collection.drop();
Service.collection.drop();
Reservation.collection.drop();
Incident.collection.drop();

async.series([
    createCommunity,
    createRole,
    createUser,
    createService,
    createReservation,
    createIncident
],
// Optional callback
function(err, results) {
    if (err) {
        console.log('FINAL ERR: '+err);
    }
    else {
        console.log('Communities: '+ communities);
        console.log('Roles: '+ roles);
        console.log('Users: '+ users);
        console.log('Services: '+ services);
        console.log('Reservations: '+ reservations);
        console.log('Incidents: '+ incidents);
        
    }
    // All done, disconnect from database
    mongoose.connection.close();
});