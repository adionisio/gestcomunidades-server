var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CommunitySchema = new Schema(
	{
		name: {type: String, required: true, max: 50},
		address: {type: String, required: true, max: 100}
	}
);

CommunitySchema
	.virtual('url')
	.get(function(){
		return '/api/v1/community/' + this._id;
	});


module.exports = mongoose.model('Community', CommunitySchema);
