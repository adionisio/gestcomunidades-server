var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
var Schema = mongoose.Schema;


var UserSchema = new Schema(
	{
		community: {type: Schema.ObjectId, ref: 'Community', required: true},
		name: {type: String, required: true, max: 50},
		surname: {type: String, required: true, max: 50},
		password: { type: String, required: true },
		email: { type: String, required: true, unique: true },
		address: {type: String, required: true, max: 100},
		floor: {type: Number, required: true },
		letter: {type: String, min:1, max:1},
		phone: {type: Number, required: true },
		role: {type: Schema.ObjectId, ref: 'Role'}
	});


UserSchema.pre('save', function(next) {
    var user = this;

    if (!user.isModified('password')) return next();
    bcrypt.genSalt(10, function(err, salt) {
        if (err) return next(err);
        bcrypt.hash(user.password, salt, function(err, hash) {
            if (err) return next(err);
            user.password = hash;
            next();
        });
    });
});

UserSchema
.virtual('url')
.get(function(){
	return '/api/v1/user/' + this._id;
});

UserSchema.methods.comparePassword = function(candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};

module.exports = mongoose.model('User', UserSchema);