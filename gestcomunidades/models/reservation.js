var mongoose = require('mongoose');
var moment = require('moment');
var Schema = mongoose.Schema;

var ReservationSchema = new Schema(
	{
		community: {type: Schema.ObjectId, ref: 'Community', required: true},
		user: {type: Schema.ObjectId, ref: 'User', required: true},
		service: {type: Schema.ObjectId, ref: 'Service', required: true},
		date_in: { type: Date, required: true },
        date_out: { type: Date, required: true }
	}
);

ReservationSchema
.virtual('url')
.get(function(){
	return '/api/v1/reservation/' + this._id;
});

// Virtual date for date_in
ReservationSchema
    .virtual('date_in_formatted')
    .get(function () {
        return this.date_in ? moment(this.date_in).format('YYYY/MM/DD HH:mm:SS') : '';
    });     

// Virtual date for date_out
ReservationSchema
    .virtual('date_out_formatted')
    .get(function () {
        return this.date_out ? moment(this.date_out).format('YYYY/MM/DD HH:mm:SS') : '';
    });     


module.exports = mongoose.model('Reservation',ReservationSchema);