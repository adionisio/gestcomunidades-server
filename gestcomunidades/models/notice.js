var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var NoticeSchema = new Schema(
	{
		community: {type: Schema.ObjectId, ref: 'Community', required: true},
		title: {type: String, min:0, max: 10, required:true},
		comment: {type: String, min:0, max:1000},
		date: { type: Date, required: true, defualt:Date.now }
	}
);

NoticeSchema
.virtual('url')
.get(function(){
	return '/api/v1/notice/' + this._id;
});


module.exports = mongoose.model('Notice',NoticeSchema);