var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var IncidentSchema = new Schema(
	{
		community: {type: Schema.ObjectId, ref: 'Community', required: true},
		user: {type: Schema.ObjectId, ref: 'User', required: true},
		title: {type: String, min:0, max: 10, required:true},
		comment: {type: String, min:0, max:1000},
		date: { type: Date, required: true, defualt:Date.now }
	}
);

IncidentSchema
.virtual('url')
.get(function(){
	return '/api/v1/incident/' + this._id;
});


module.exports = mongoose.model('Incident',IncidentSchema);