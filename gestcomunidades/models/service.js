var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TimetableSchema = new Schema(
	{
		open_hour: {type: Number, min:0, max:23},
		open_minute: {type: Number, min:0, max:59},
		close_hour: {type: Number, min:0, max:23},
		close_minute: {type: Number, min:0, max:59}		
	}
);


var ServiceSchema = new Schema(
	{
		community: {type: Schema.ObjectId, ref: 'Community', required: true},
		name: {type: String, required: true, max: 50},
		timetable: TimetableSchema,
		bookable: {type: Boolean, required: true, default: false}
	}
);

ServiceSchema
.virtual('url')
.get(function(){
	return '/api/v1/service/' + this._id;
});

// Virtual date for date
ServiceSchema
    .virtual('date_formatted')
    .get(function () {
        return this.date_in ? moment(this.date).format('YYYY/MM/DD HH:mm:SS') : '';
    });     


module.exports = mongoose.model('Service',ServiceSchema);