var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var RoleSchema = new Schema(
	{
		category: {type: String, required: true, unique:true, max: 20},
	}
);

RoleSchema
.virtual('url')
.get(function(){
	return '/api/v1/role/' + this._id;
});


module.exports = mongoose.model('Role',RoleSchema);
