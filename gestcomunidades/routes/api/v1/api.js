var express = require('express');
var router = express.Router();
var multer  = require('multer');
var imgIncidentsPath = 'public/images/incidents/';
var uploadIncidents = multer({ dest: imgIncidentsPath });

var user_controller = require('../../../controllers/api/v1/userController');
var community_controller = require('../../../controllers/api/v1/communityController');
var role_controller = require('../../../controllers/api/v1/roleController');
var service_controller = require('../../../controllers/api/v1/serviceController');
var reservation_controller = require('../../../controllers/api/v1/reservationController');
var incident_controller = require('../../../controllers/api/v1/incidentController');
var notice_controller = require('../../../controllers/api/v1/noticeController');


router.post('/authenticate/user', user_controller.user_token_authenticate);
router.use(user_controller.check_token);

// User
router.get('/user', user_controller.user_list);
router.post('/user', user_controller.user_create);
router.get('/user/:id', user_controller.user_detail);
router.get('/community/:id/user', user_controller.users_by_community);
router.get('/community/:id/user/administrator', user_controller.user_detail_administrator);
router.get('/community/:id/user/doorman', user_controller.user_detail_doorman);
router.delete('/user/:id', user_controller.user_delete);

// Community
router.get('/community', community_controller.community_list);
router.post('/community', community_controller.community_create);
router.get('/community/:id', community_controller.community_detail);
router.delete('/community/:id', community_controller.community_delete);

// Role
router.get('/role', role_controller.role_list);
router.post('/role', role_controller.role_create);
router.get('/role/:id', role_controller.role_detail);
router.delete('/role/:id', role_controller.role_delete);

// Service
router.get('/service', service_controller.service_list);
router.post('/service', service_controller.service_create);
router.get('/service/:id', service_controller.service_detail);
router.get('/community/:id/service', service_controller.services_by_community);
router.get('/community/:id/reservation/service', service_controller.services_bookable_by_community);
router.delete('/service/:id', service_controller.service_delete);

// Reservation
router.get('/reservation', reservation_controller.reservation_list);
router.post('/reservation', reservation_controller.reservation_create);
router.get('/reservation/:id', reservation_controller.reservation_detail);
router.get('/user/:id/reservation', reservation_controller.reservations_by_user);
router.get('/community/:id/reservation', reservation_controller.reservations_by_community);
router.get('/community/:id/service/:service/reservation', reservation_controller.check_reservations_by_community_service_date);
router.get('/community/:id/service/:service/reservation/:date', reservation_controller.reservations_by_community_service_date);
router.delete('/reservation/:id', reservation_controller.reservation_delete);

// Incident
router.get('/incident', incident_controller.incident_list);
router.post('/incident', incident_controller.incident_create);
router.get('/incident/:id', incident_controller.incident_detail);
router.get('/community/:id/incident', incident_controller.incident_by_community);
router.get('/community/:id/user/:iduser/incident', incident_controller.incident_by_community_user);
router.delete('/incident/:id', incident_controller.incident_delete);
router.post('/incident/photo', uploadIncidents.single('img_user'), incident_controller.incident_create_photo);

//Notice
router.get('/notice', notice_controller.notice_list);
router.post('/notice', notice_controller.notice_create);
router.get('/notice/:id', notice_controller.notice_detail);
router.get('/community/:id/notice', notice_controller.notices_by_community);
router.delete('/notice/:id', notice_controller.notice_delete);

module.exports = router;