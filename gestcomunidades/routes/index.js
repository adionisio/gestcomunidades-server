var express = require('express');
var router = express.Router();
var User = require('../models/user');
var Incident = require('../models/incident');
var user_controller = require('../controllers/userController');
var service_controller = require('../controllers/serviceController');
var notice_controller = require('../controllers/noticeController');
var reservation_controller = require('../controllers/reservationController');
var incident_controller = require('../controllers/incidentController');

var multer  = require('multer');
var imgProfilePath = 'public/images/users/';
var upload = multer({ dest: imgProfilePath });



/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Gestcomunidades' });
});

router.get('/login', user_controller.login_form);
router.post('/login', user_controller.login);
router.use(user_controller.check_token);
router.get('/user', user_controller.user_detail);
router.get('/residents', user_controller.users_in_community);
router.get('/services', service_controller.services_in_community);
router.get('/notices', notice_controller.notices_in_community);
router.get('/reservations', reservation_controller.reservations);
router.post('/reservations', reservation_controller.reservations_post);
router.get('/incidents', incident_controller.incidents_in_community);
router.get('/upload_user_image', user_controller.upload_user_image);
router.post('/upload_user_image', upload.single('img_user'),user_controller.upload_user_image_post);
router.get('/logout', user_controller.logout);




module.exports = router;
